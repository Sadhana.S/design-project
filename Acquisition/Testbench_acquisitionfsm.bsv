import acquisitionfsm::*;

module mkTestbench();

    Reg#(Bit#(2)) flag <- mkReg(0);
    Reg#(Bit#(5)) rg_prn <- mkReg(0);
    Ifc_acquisitionfsm acquisitionfsm <- mkacquisitionfsm;

    rule r0(flag == 0);
        acquisitionfsm.initialise;
        flag <= 1;
    endrule
    rule r1((rg_prn <= 31)&&(flag == 1));
        acquisitionfsm.prn_value(rg_prn);
        flag <= 2;
    endrule
    rule r2(flag == 2);
        let acq_status = acquisitionfsm.acquisition_status;
        let code_shift = acquisitionfsm.code_phase;
        let freq_error = acquisitionfsm.frequency_error;
        //$display($time,"From testbench : %b, %d and %d \n",acq_status,code_shift,freq_error);
        rg_prn <= rg_prn + 1;
        flag <= 1;
        if(rg_prn == 31)
            $finish;
    endrule
endmodule : mkTestbench