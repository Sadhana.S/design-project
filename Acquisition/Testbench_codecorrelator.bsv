import codecorrelator::*;

module mkTestbench();

    Ifc_codecorrelator codecorrelator <- mkcodecorrelator;

    Reg#(Bit#(2)) rg_flag <- mkReg(0);
    Reg#(Bit#(11)) rg_count <- mkReg(0);

    rule r1_tb(rg_flag == 0);
        $display($time," r1_tb \n");
        codecorrelator.initialise();
        rg_flag <= 1;
    endrule
    rule r2_tb(rg_flag == 1);
        Int#(24) a = codecorrelator.result_firstmax();
        Int#(24) b = codecorrelator.result_secondmax();
        Bit#(11) c = codecorrelator.result_firstmax_code();
        Bit#(11) d = codecorrelator.result_secondmax_code();
        $display($time," First_max = %d corresponding code phase = %d \n Second_max = %d corresponding code phase = %d \n",a,c,b,d);
        $finish;
    endrule

endmodule : mkTestbench