import cordicpipe_acq::*;
import freqsynthesiser::*;
import Real::*;
import Vector::*;
import FixedPoint::*;

typedef 12000 Samples;
typedef 500 Interval;

module mkTestbench();

    Integer samp1 = valueOf(Samples);
    Bit#(16) samp = fromInteger(samp1);
    Integer interval1 = valueOf(Interval);
    Int#(16) interval = fromInteger(interval1);
    FixedPoint#(12,20) scalled_samp_freq = 357.913941333333;

    Ifc_cordicpipe_acq ifc_tb1 <- mkcordicpipe_acq;
    Ifc_freqsynthesiser ifc_tb2 <- mkfreqsynthesiser;

    Reg#(Bit#(11)) count1 <- mkReg(0);
    Reg#(Int#(5)) flag <- mkReg(0);
    Reg#(Bit#(2)) readFlag <- mkReg(0);

    rule ro(flag == 0);
        //Bit#(16) a = fromInteger((2*stop_freq)/interval1);
        FixedPoint#(24,8) start = 3563000;
        ifc_tb2.get_inputs(500,scalled_samp_freq,start,0);
        flag <= 1;
    endrule
    rule r1(flag == 1);
        Int#(33) a = ifc_tb2.results_base();
        Int#(33) b = ifc_tb2.results_interval();
        $display($time,"Base angle : %d\n",a);
        ifc_tb1.initialise(a,b);
        flag <= 2;
    endrule
    rule r2((flag==2)&&(count1<2046));
        ifc_tb1.bram_read(count1);
        count1 <= count1+1;
        if(count1==0)
            readFlag <= 1;
    endrule
    rule r3(readFlag == 1);
        let cosine <- ifc_tb1.bram_i;
        let sine <- ifc_tb1.bram_q;
        $display($time," %d : From testbench : I_out = %d and Q_out = %d \n",count1,cosine,sine);
        if(count1 == 2046)
        begin
            readFlag <= 0;
            $finish;
        end
    endrule
endmodule : mkTestbench