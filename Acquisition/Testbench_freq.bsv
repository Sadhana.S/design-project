	import freqsynthesiser ::*;
	import Vector ::*;
	import Real::*;
	import FixedPoint::*;

module mkTestbench();

	Reg#(Int#(8)) frqbin_index <- mkReg(20);
	Reg#(Int#(16)) interval <- mkReg(500);
	Reg#(FixedPoint#(12,20)) scalled_samp_freq <- mkReg(262.406662919426);//12 MHz
	Reg#(FixedPoint#(24,8)) base_freq <- mkReg(4120400); //-10 KHz
	Reg#(Int#(33)) phase <- mkReg(0);
	Reg#(Bit#(3)) flag <- mkReg(0);
	Reg#(Int#(33)) angle <- mkReg(0);
	Reg#(Int#(7)) rg_count <- mkReg(0);

	Ifc_freqsynthesiser ifc_tb <- mkfreqsynthesiser;
	
	rule r1_give_inputs(flag == 0);
		ifc_tb.get_inputs(interval, scalled_samp_freq,base_freq,phase);
		flag <= 1;
	endrule
	
	rule r2_get_results(flag == 1);
		let a = ifc_tb.results_base();
		let b = ifc_tb.results_interval();
		$display("\n base angle = %d",a);
		$display("\n Interval angle = %d",b);
		flag <= 2;
	endrule
	rule r3(flag ==2);
		ifc_tb.get_inputs(interval, scalled_samp_freq,4140400,phase);
		flag <= 3;
	endrule
	rule r4(flag == 3);
		let a = ifc_tb.results_base();
		let b = ifc_tb.results_interval();
		$display("\n base angle = %d",a);
		$display("\n Interval angle = %d",b);
		$finish;
	endrule

endmodule : mkTestbench