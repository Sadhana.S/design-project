import magnitudepipe::*;

module mkTestbench();

    Ifc_magnitudepipe ifc_tb <- mkmagnitudepipe;

    Reg#(Bit#(3)) flag <- mkReg(0);
    Reg#(Int#(32)) i <- mkReg(24576);//0.75
    Reg#(Int#(32)) q <- mkReg(14090); //0.43

    rule r1(flag==0);
        $display("\n Entered r1");
        ifc_tb.get_inputs(24576,14090);
        flag <= 1;
    endrule
    rule r2(flag==1);
        Int#(32) b <- ifc_tb.result_magnitude();
        $display("\n For I_P = 24576 and Q_P = 14090, magnitude = %d",b);
        flag <= 2;
    endrule
    rule r3(flag==2);
        $display("\n Entered r3");
        ifc_tb.get_inputs(128,128);
        flag <= 3;
    endrule
    rule r4(flag==3);
        Int#(32) b <- ifc_tb.result_magnitude();
        $display("\n For I_P = 128 and Q_P = 128, magnitude = %d",b);
        $finish;
    endrule

endmodule : mkTestbench