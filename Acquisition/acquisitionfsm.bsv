/*
Module: Acquisition FSM
Description: This module implements the Acquisition FSM. The state machine is as follows:
	1. Generate scaled versions of the frequencies which are required for freq. shifting.
	   In the first cycle, the first input (-10kHz) is sent to the freq synthesizer.
	2. In the second cycle, the base angle and the interval angle are read from the freq
	   synthesizer, and are sent to the freq shift block. This block would then read the sample data
	   and perform freq. shifting on each sample.
	3. Third cycle onwards, for the next `samp_freq number of cycles, the outputs of the freq shift
	   block are read, and are fed to the code shift module, which in turn does the correlation.
	4. Once correlation is done for this particular freq shift, the freq shift module is again
	   called to perform freq shift on the sample data for the next freq shift.
	5. The next cycle onwards the new freq shifted data is sent to the code shift block.
	6. This happens untill all freq. shift have been performed.
    7. ?

The code is written for 32 PRN values (32 satellite) and the ouput from RF is in Intermediate Frequency(IF)
*/

package acquisitionfsm;

    import freqsynthesiser ::*;//Provides scalled version of the base frequency(IF - 10K) and interval frequency
    import codecorrelator::*;//Performs circular correlation of the frequenct shifted values with respective PRN
    import Vector::*;
    import FixedPoint::*;

    interface Ifc_acquisitionfsm;
        method Action initialise;
        method Action prn_value(Bit#(5) prn_no);
        method Bool acquisition_status;
        method Bit#(11) code_phase;
        method Bit#(7) frequency_error;
    endinterface : Ifc_acquisitionfsm

    module mkacquisitionfsm(Ifc_acquisitionfsm);

        Reg#(Bit#(3)) rg_flag <- mkReg(0);
        Reg#(Bit#(7)) rg_freq_shift_count <- mkReg(0);
        Reg#(Bit#(6)) rg_prn_count <- mkReg(0);
        Reg#(Bit#(5)) rg_prn <- mkReg(0);

        Vector#(32,Reg#(Bool)) acq_state <- replicateM(mkReg(False));
        Vector#(32,Reg#(Bit#(7))) frequency_shift <- replicateM(mkReg(0));
        Vector#(32,Reg#(Bit#(11))) firstmax_code <- replicateM(mkReg(0));
        Vector#(32,Reg#(Bit#(11))) secondmax_code <- replicateM(mkReg(0));
        Vector#(32,Reg#(Int#(32))) firstmax <- replicateM(mkReg(0));
        Vector#(32,Reg#(Int#(32))) secondmax <- replicateM(mkReg(0));

        Ifc_freqsynthesiser freqsynthesiser <- mkfreqsynthesiser;
        Ifc_codecorrelator codecorrelator <- mkcodecorrelator;
        
        // Inputs to frequency synthesiser such as scalled sampling frequency, intermediate frequency and doppler interval
        rule r1_inputs_to_freqsynthesiser(rg_flag == 1);
            //FixedPoint#(24,8) start_freq = 3553000; //3563000(IF)-10000(10K)
            FixedPoint#(24,8) start_freq = 4120400; //4130400(IF) - 10000(10K)
            freqsynthesiser.get_inputs(500,262.406662919426,start_freq,0);
            rg_flag <= 2;
        endrule
        // Outputs of frequency synthesiser is given to cordicpipe via code correlator
        rule r2_outputs_from_freqsynthesiser_and_initialise_cordicpipe(rg_flag == 2);
            Int#(33) base_angle = freqsynthesiser.results_base;
            Int#(33) interval_angle = freqsynthesiser.results_interval;
            codecorrelator.initialise_cordic(base_angle,interval_angle);
            rg_flag <= 3;
        endrule
        // Initialising code shift correlator - rg_prn_count will seek the starting address of the PRN value 
        rule r3_initialising_codecorrelator((rg_prn_count < 32)&&(rg_flag == 3));
            codecorrelator.initialise(rg_prn_count);
            rg_flag <= 4;
        endrule
        // Outputs from code correlator are obtained and updating the values based on the first maximum
        rule r4_outputs_from_code_correlator(rg_flag == 4);
            Int#(32) a = codecorrelator.result_firstmax();
            Int#(32) b = codecorrelator.result_secondmax();
            Bit#(11) c = codecorrelator.result_firstmax_code();
            Bit#(11) d = codecorrelator.result_secondmax_code();
            if(a > firstmax[rg_prn_count])
            begin
                frequency_shift[rg_prn_count] <= rg_freq_shift_count;//complete it
                firstmax[rg_prn_count] <= a;
                secondmax[rg_prn_count] <= b;
                firstmax_code[rg_prn_count] <= c;
                secondmax_code[rg_prn_count] <= d;
            end
            $display($time," %d %d First_max = %d corresponding code phase = %d \t Second_max = %d corresponding code phase = %d \n",rg_freq_shift_count,rg_prn_count,a,c,b,d);
            rg_flag <= 3;
            rg_prn_count <= rg_prn_count + 1;
        endrule
        // Reinitialising cordic pipe for next frequency shift as well as reinitialisation of PRN value and incrementing the frequency bin index
        rule r5_reinitialise_cordipipe((rg_prn_count == 32)&&(rg_freq_shift_count <= 40));
            if(rg_freq_shift_count == 40)
                rg_flag <= 5;
            else
            begin
                codecorrelator.reinitialise_cordic;
                rg_flag <= 3;
            end
            rg_prn_count <= 0;
            rg_freq_shift_count <= rg_freq_shift_count + 1;
        endrule
        //After completion of code shifts for all frequency shifts, acquisition ratio is calculated to say whether a satellite is acquired
        //Second_maximum is multiplied by the acquisition threshold and compared with first maximum. If first maximum is greater then the satellite is acquired
        rule r6_acquisition_ratio_computation(rg_flag == 5);
            FixedPoint#(32,8) temp1 = fromInt(secondmax[rg_prn_count]);
            FixedPoint#(2,8) temp2 = 1.42;
            Int#(34) temp3 = fxptGetInt(fxptMult(temp1,temp2));
            Int#(34) temp4 = zeroExtend(firstmax[rg_prn_count]);
            if(temp4 > temp3)
            begin
                acq_state[rg_prn_count] <= True;
                $display($time," Satellite %d is acquired with code phase = %d and freq_bin = %d ",rg_prn_count+1,firstmax_code[rg_prn_count],frequency_shift[rg_prn_count]);
            end
            rg_prn_count <= rg_prn_count + 1;
            if(rg_prn_count == 31)
                rg_flag <= 6;
        endrule

        method Action initialise;
            rg_flag <= 1;
        endmethod
        method Action prn_value(Bit#(5) prn_no) if(rg_flag == 6);
            rg_prn <= prn_no;
        endmethod
        method Bool acquisition_status;
            return acq_state[rg_prn];
        endmethod
        method Bit#(11) code_phase;
            return firstmax_code[rg_prn];
        endmethod
        method Bit#(7) frequency_error;
            return frequency_shift[rg_prn];
        endmethod
    endmodule : mkacquisitionfsm

endpackage : acquisitionfsm