/*
Module : code shift correlator
Description:
Input : Frequency shifted I and Q samples from cordicpipe_acq which is stored in BRAM and the PRN number which will be an offset in prn_value BRAMLoad text file
Output: The first and second maximum along with their code phases.

1. For 'n' parallel correlators, 2048/n(~2046/n) systolic iterations are done
2. Each Systolic iteration have initial PRN fetch (n fetches) and 2046 processing cycles where one input sample at a time is given every clock cycle
3. At the end of each systolic iteration, 'n' correlation outputs (I and Q) (result of n - PRN shifts) will be obtained which is then sent to the pipelined magnitude computation (sqrt(I^2+Q^2))
4. After the initial latency of magnitude computator, the outputs are obtained for every clock cycle which are compared to update the first, second and third maximum
*/
package codecorrelator;

import magnitudepipe::*;
import cordicpipe_acq::*;
import Vector::*;
import BRAMCore::*;

interface Ifc_codecorrelator;
    method Action initialise_cordic(Int#(33) base_in, Int#(33) interval_in);
    method Action reinitialise_cordic;
    method Action initialise(Bit#(6) prn_no);
    method Int#(32) result_firstmax();
    method Bit#(11) result_firstmax_code();
    method Int#(32) result_secondmax();
    method Bit#(11) result_secondmax_code();
endinterface : Ifc_codecorrelator

module mkcodecorrelator(Ifc_codecorrelator);

    Integer prn = 32768;

    Reg#(Bit#(3)) rg_flag <- mkReg(0);//for the state transition in FSM
    Reg#(Bit#(1)) rg_writeflag <- mkReg(0);//for the start an stop of writing values to magnitude computation pipe
    Reg#(Bit#(7)) rg_count_initial <- mkReg(0);//
    Reg#(Bit#(11)) rg_count_super <- mkReg(0);
    Reg#(Bit#(7)) rg_count_write <- mkReg(0);
    Reg#(Bit#(11)) rg_count_compare <- mkReg(0);

    Reg#(Bit#(15)) rg_initial_prn <- mkReg(0);
    Reg#(Bit#(15)) rg_current_prn <- mkReg(0);
    Reg#(Bit#(15)) rg_last_prn <- mkReg(0);

    Reg#(Int#(32)) rg_current_I <- mkReg(0);
    Reg#(Int#(32)) rg_current_Q <- mkReg(0);

    Vector#(64,Reg#(Int#(32))) accumulation_I_cell <- replicateM(mkReg(0));
    Vector#(64,Reg#(Int#(32))) accumulation_Q_cell <- replicateM(mkReg(0));
    Vector#(64,Reg#(Bit#(1))) prn_cell <- replicateM(mkReg(0));

    BRAM_PORT#(Bit#(15),Bit#(1)) prn_val <- mkBRAMCore1Load(prn,False,"/home/sadhana/Desktop/NavIC_correlator/src/prn_value",False);
    Reg#(Bit#(11)) rg_count_iter <- mkReg(2);

    Reg#(Int#(32)) rg_firstmax <- mkReg(0);
    Reg#(Bit#(11)) rg_firstmax_code <- mkReg(0);
    Reg#(Int#(32)) rg_secondmax <- mkReg(0);
    Reg#(Bit#(11)) rg_secondmax_code <- mkReg(0);
    Reg#(Int#(32)) rg_thirdmax <- mkReg(0);
    Reg#(Bit#(11)) rg_thirdmax_code <- mkReg(0);

    Ifc_magnitudepipe magnitudepipe <- mkmagnitudepipe;
    Ifc_cordicpipe_acq cordicpipe_acq <- mkcordicpipe_acq;

    //Initial PRN fetch for 64 cells
    rule r1_initialise_prn_cells(rg_flag == 1);
        prn_cell[rg_count_initial] <= prn_val.read;
        prn_cell[rg_count_initial+1] <= prn_val.read;
        //$display($time,"%d %d prn = %d",rg_count_super,rg_count_initial,rg_initial_prn);
        prn_val.put(False,rg_initial_prn,?);
        if(rg_initial_prn == rg_last_prn)
            rg_initial_prn <= rg_last_prn - 1022;
        else
            rg_initial_prn <= rg_initial_prn + 1;
        if(rg_count_initial == 62)
        begin
            rg_flag <= 2;
            rg_current_prn <= rg_initial_prn;
            let i <- cordicpipe_acq.bram_i;
            let q <- cordicpipe_acq.bram_q;
            rg_current_I <= signExtend(i);
            rg_current_Q <= signExtend(q);
            cordicpipe_acq.bram_read(1);
            rg_count_initial <= 0;
            //$display($time," %d Initialisation of PRN values done\n",rg_count_super);
        end
        else
            rg_count_initial <= rg_count_initial + 2;
    endrule
    //Systolic processing which is repeated for 2046 cycles (size of input)
    for(Bit#(7) i=0; i<=63; i=i+1)
    begin
        rule r2_processing_iterations((rg_flag == 2)&&(rg_writeflag == 0));
            if(prn_cell[i] == 1)
            begin
                accumulation_I_cell[i] <= accumulation_I_cell[i] + rg_current_I;
                accumulation_Q_cell[i] <= accumulation_Q_cell[i] + rg_current_Q;
            end
            else
            begin
                accumulation_I_cell[i] <= accumulation_I_cell[i] - rg_current_I;
                accumulation_Q_cell[i] <= accumulation_Q_cell[i] - rg_current_Q;
            end
            if(i!=63)
                prn_cell[i] <= prn_cell[i+1];
        endrule
    end
    //PRN fetch for the last systolic cell for every iteration
    rule r3_prn_update((rg_flag == 2)&&(rg_writeflag == 0));
        prn_cell[63] <= prn_val.read;
        let i <- cordicpipe_acq.bram_i;
        let q <- cordicpipe_acq.bram_q;
        rg_current_I <= signExtend(i);
        rg_current_Q <= signExtend(q);
        let temp_1 = rg_current_prn + zeroExtend(rg_count_iter[0]);
        if(temp_1 == rg_last_prn + 1)
        begin
            Bit#(15) temp_2 = rg_last_prn -1022;
            rg_current_prn <= temp_2;
            //$display($time,"%d %d prn = %d",rg_count_super,rg_count_iter,temp_2);
            prn_val.put(False,temp_2,?);
        end
        else
        begin
            rg_current_prn <= temp_1;
            //$display($time,"%d %d prn = %d",rg_count_super,rg_count_iter,temp_1);
            prn_val.put(False,temp_1,?);
        end
        if(rg_count_iter == 2047)
        begin
            rg_flag <= 3;
            rg_writeflag <= 1;
            //$display($time," %d Systolic processing is done\n",rg_count_super);
        end
        else
        begin
            cordicpipe_acq.bram_read(rg_count_iter);
            rg_count_iter <= rg_count_iter + 1;
        end
    endrule
    //At the end of 64 correlations, I and Q values of each systolic cell is sent to pipelined magnitude computation
    rule r4_magnitude_computation(rg_writeflag == 1);
        magnitudepipe.get_inputs(accumulation_I_cell[rg_count_write],accumulation_Q_cell[rg_count_write]);
        //$display($time,"%d %d : I=%d and Q = %d",rg_count_super,rg_count_write,accumulation_I_cell[rg_count_write],accumulation_Q_cell[rg_count_write]);
        accumulation_I_cell[rg_count_write] <= 0;
        accumulation_Q_cell[rg_count_write] <= 0;
        if(rg_count_write == 63)
        begin
            rg_writeflag <= 0;
            rg_count_write <= 0;
            //$display($time," %d Inputs are given for magnitude computation\n",rg_count_super);
        end
        else
            rg_count_write <= rg_count_write + 1;
    endrule
    //Compare the 64 magnitudes and update first, second and third maximum
    rule r5_compare_magnitude;
        Int#(32) a <- magnitudepipe.result_magnitude();
        //$display($time," %d %d \n",rg_count_compare,a);
        if(a>rg_firstmax)
        begin
            rg_firstmax <= a;
            rg_firstmax_code <= rg_count_compare;
            rg_secondmax <= rg_firstmax;
            rg_secondmax_code <= rg_firstmax_code;
            rg_thirdmax <= rg_secondmax;
            rg_thirdmax_code <= rg_secondmax_code;
        end
        else if(a>rg_secondmax)
        begin
            rg_secondmax <= a;
            rg_secondmax_code <= rg_count_compare;
            rg_thirdmax <= rg_thirdmax;
            rg_thirdmax_code <= rg_thirdmax_code;
        end
        else if(a>rg_thirdmax)
        begin
            rg_thirdmax <= a;
            rg_thirdmax_code <= rg_count_compare;
        end
        rg_count_compare <= rg_count_compare + 1;
        if(rg_count_compare == 2045)
        begin
            rg_flag <= 5;
            //$display($time," %d Magnitude comparison is done\n",rg_count_super);
        end
    endrule
    // reinitialisation for next systolic prn fetch and processing
    rule r6_reset(rg_flag == 3);
        if(rg_count_super == 31)
            rg_flag <= 4;
        else
        begin
            cordicpipe_acq.bram_read(0);
            rg_count_initial <= 0;
            rg_count_iter <= 2;
            rg_count_super <= rg_count_super + 1;
            rg_flag <= 1; 
            //$display($time," %d Re-initialisation\n",rg_count_super);
        end
    endrule
    //As first and second maximum should not belong to same PRN, if the first and second maximum codes differs by 1, then the second maximum is updated by the third one 
    rule r7_max_decision(rg_flag == 5);
        if((rg_firstmax_code - rg_secondmax_code == 1)||(rg_secondmax_code - rg_firstmax_code == 1))
        begin
            rg_secondmax <= rg_thirdmax;
            rg_secondmax_code <= rg_thirdmax_code;
        end
        rg_flag <= 6;
    endrule
    method Action initialise_cordic(Int#(33) base_in, Int#(33) interval_in);
        cordicpipe_acq.initialise(base_in,interval_in);
    endmethod
    method Action reinitialise_cordic;
        cordicpipe_acq.reinitialise;
    endmethod
    method Action initialise(Bit#(6) prn_no);
        rg_flag <= 1;
        rg_count_super <= 0;
        rg_count_initial <= 0;
        rg_count_iter <= 2;
        rg_count_compare <= 0;
        Bit#(15) temp = zeroExtend(prn_no)*1023;
        prn_val.put(False,temp,?);
        cordicpipe_acq.bram_read(0);
        rg_initial_prn <= temp + 1;
        rg_last_prn <= temp + 1022;
        rg_firstmax <= 0;
        rg_firstmax_code <= 0;
        rg_secondmax <= 0;
        rg_secondmax_code <= 0;
        rg_thirdmax <= 0;
        rg_thirdmax_code <= 0;
    endmethod
    method Int#(32) result_firstmax() if(rg_flag == 6);
        return rg_firstmax;
    endmethod
    method Bit#(11) result_firstmax_code() if(rg_flag == 6);
        return rg_firstmax_code;
    endmethod
    method Int#(32) result_secondmax() if(rg_flag == 6);
        return rg_secondmax;
    endmethod
    method Bit#(11) result_secondmax_code() if(rg_flag == 6);
        return rg_secondmax_code;
    endmethod

endmodule : mkcodecorrelator

endpackage : codecorrelator