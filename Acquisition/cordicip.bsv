package cordicip;

import Vector ::*;
import Real ::*;
import FixedPoint::*;
import BRAM::*;
import BRAMCore::*;
import GetPut::*;
import ClientServer::*;

typedef 10 Samples;     //Number of samples per millisecond //12000
typedef 16 Iterations;  //Number of cordic iterations
typedef 17 Iterations1; //Iterations + 1
typedef 32768 Scale;    //Required scalling factor*16

interface Ifc_cordicip;
    //Get the scaled base angle, scaled interval value, and number of frequency shifts that need to be performed
    method Action initialise(Int#(33) base_in, Int#(33) interval_in, Bit#(6) count);//Initialisation of cordic table
    method Action next_iter;//Control signal to start next iteration of next frequency shift (mostly given after reading BRAM values)
    method Action readbram(Bit#(16) address);//Provide address for reading values from BRAM
    method ActionValue#(Int#(30)) bramcosine;//I' value
    method ActionValue#(Int#(30)) bramsine;//Q' value
endinterface : Ifc_cordicip

function BRAMRequest#(Bit#(16), Int#(30)) makeRequest(Bool write, Bit#(16) addr, Int#(30) data);
    return BRAMRequest{
    write: write,
    responseOnWrite:False,
    address: addr,
    datain: data
    };
 endfunction

module mkcordicip(Ifc_cordicip);

    Integer samp1 = valueOf(Samples);
    Bit#(16) samp = fromInteger(samp1);
    Integer iterations = valueOf(Iterations);
    Bit#(16) cordic_iter = fromInteger(iterations);
    Integer scale = valueOf(Scale);
    Int#(21) scale1 = fromInteger(scale);

    Vector#(31,Reg#(Int#(32))) atan_table <- replicateM(mkReg(0));

    BRAM_Configure cfg1 = defaultValue;
    BRAM1Port#(Bit#(16),Int#(30)) sine <- mkBRAM1Server(cfg1);
    BRAM1Port#(Bit#(16),Int#(30)) cosine <- mkBRAM1Server(cfg1);
    Reg#(Bit#(16)) count1 <- mkReg(0);

    BRAM_PORT#(Bit#(16),Int#(12)) i_val <- mkBRAMCore1Load(samp1,False,"/home/sadhana/Desktop/NavIC_correlator/src/i_value",False);
    BRAM_PORT#(Bit#(16),Int#(12)) q_val <- mkBRAMCore1Load(samp1,False,"/home/sadhana/Desktop/NavIC_correlator/src/q_value",False);
    
    Reg#(Bit#(16)) count2 <- mkReg(0);
    Reg#(Bit#(6)) count3 <- mkReg(0);
 
    //Reg#(FixedPoint#(21,1)) mult1 <- mkReg(524288);
    Reg#(FixedPoint#(1,14)) mult2 <- mkReg(0.60725320630464);
    Reg#(Int#(33)) angle <- mkReg(0);
    Reg#(Int#(33)) angle_base <- mkReg(0);
    Reg#(Int#(33)) base <- mkReg(0);
    Reg#(Bit#(6)) iter <- mkReg(0);
    Reg#(Int#(12)) i_in <- mkReg(0);
    Reg#(Int#(12)) q_in <- mkReg(0);
	Reg#(Int#(34)) x <- mkReg(0);
	Reg#(Int#(34)) y <- mkReg(0);
	Reg#(Int#(32)) z <- mkReg(0);
	Reg#(Int#(34)) temp1 <- mkReg(0);
	Reg#(Int#(34)) temp2 <- mkReg(0);

    Reg#(Bit#(3)) flag <- mkReg(0);
    Reg#(Bit#(1)) readFlag <- mkReg(0);
    Reg#(Bit#(1)) proceed <- mkReg(0);

    rule r0_get_inputs1((readFlag==1)&&(flag==0));
            i_val.put(False,count1,?);
            q_val.put(False,count1,?);
            let temp = angle + angle_base;
            angle<=temp;
            flag <= 1;
    endrule

    rule r1_get_inputs2(flag == 1);
        i_in <= i_val.read;
        q_in <= q_val.read;
        flag <= 2;
    endrule

    //TODO combine the above rule and this one.
    //TODO think if the scaling factor (mult2) is required at all because the computation that we are doing is ka+kb+kc... 
    //     Since k is common, and since we are not bothered about absolute values, can remove this multiplication altogether
    //     In cordic the mul factor exists to obtain the actual value of sine and cos.
    rule r2_add_guards(flag == 2);
    $display($time, " Input : count3=%d,count1=%d,i_in=%d,q_in=%d and angle_in=%d \n",count3,count1,i_in,q_in,angle);
        FixedPoint#(12,1) i2 = fromInt(i_in);
        FixedPoint#(12,1) q2 = fromInt(q_in);
        FixedPoint#(13,15) i3 = fxptMult(i2,mult2);
        FixedPoint#(13,15) q3 = fxptMult(q2,mult2);
        
        //The scaling factor that is needed to change it to an Integer. fexptMult is not required. Instead shifting would suffice.
        //The amount of left shift = Log(base 2)(scale1).
        //TODO instead of mul, use shifting (by constant value) and then use fxptGetInt
        FixedPoint#(21,1) mult1 = fromInt(scale1);
        x <= fxptGetInt(fxptMult(i3,mult1));
        y <= fxptGetInt(fxptMult(q3,mult1));
        flag <= 3;
    endrule

    rule r3_converge_angle(flag == 3);
        Int#(34) i4 = 0;
        Int#(34) q4 = 0;
        Int#(32) a3 = 0;
        if((angle > 1073741824)&&(angle <= 2147483648)) //second quadrant
        begin
            i4 = -y;
            q4 = x;
            a3 = truncate(angle - 1073741824);
        end
        else if((angle > 2147483648)&&(angle <= 3221225472)) //third quadrant
                    begin
                        i4 = y;
                        q4 = -x;
                        a3 = truncate(angle + 1073741824);
                    end
                    else
                        begin //first and fourth quadrant
                            i4 = x;
                            q4 = y;
                            a3 = truncate(angle);
                        end
		x <= i4;
		y <= q4;
        z <= a3;
        flag <= 4;
    endrule
	
    rule r4_cordic_iterations((flag == 4)&&(count2 < cordic_iter));
        if(z > 0)
        begin
            temp1 <= x - (y >> count2);
            temp2 <= y + (x >> count2);
            z <= z - atan_table[count2];
        end
        else
        begin
            temp1 <= x + (y >> count2);
            temp2 <= y - (x >> count2);
            z <= z + atan_table[count2];
        end
        flag <= 5;
    endrule

    //TODO this rule may not be required. Instead use values of temp1 and temp2 in rule "r4_push_output"
    rule r5_cordic_update(flag == 5);
        x <= temp1;
        y <= temp2;
        count2 <= count2+1;
        flag <= 4;
    endrule

    rule r4_push_output(count2 == cordic_iter);     
        Int#(30) i_out = truncate(x>>4);
        Int#(30) q_out = truncate(y>>4);
        cosine.portA.request.put(makeRequest(True,count1,i_out));
        sine.portA.request.put(makeRequest(True,count1,q_out));
        count1 <= count1+1;
        count2 <= 0;
        if(count1 < samp-1)
            flag <= 0;
    endrule
    
    rule r5_re_initialise((proceed == 1)&&(count3 < iter-1));
        $display($time," Starting iteration : %d \n",count3+1);
        count3 <= count3 +1;
        angle_base <= angle_base + base;
        angle <= 0;
        count1 <= 0;
        count2 <= 0;
        proceed <= 0;
        flag <= 0;
    endrule
    rule r6_next_input((count3 == iter-1)&&(count1 == samp));
        readFlag <= 0;
    endrule

    method Action initialise(Int#(33) base_in,Int#(33) interval_in, Bit#(6) count) if(readFlag == 0);
//Feedback angles for cordic
        atan_table[00] <= 'b00100000000000000000000000000000; // 45.000 degrees -> atan(2^0)
        atan_table[01] <= 'b00010010111001000000010100011101; // 26.565 degrees -> atan(2^-1)
        atan_table[02] <= 'b00001001111110110011100001011011; // 14.036 degrees -> atan(2^-2)
        atan_table[03] <= 'b00000101000100010001000111010100; // atan(2^-3)
        atan_table[04] <= 'b00000010100010110000110101000011;
        atan_table[05] <= 'b00000001010001011101011111100001;
        atan_table[06] <= 'b00000000101000101111011000011110;
        atan_table[07] <= 'b00000000010100010111110001010101;
        atan_table[08] <= 'b00000000001010001011111001010011;
        atan_table[09] <= 'b00000000000101000101111100101110;
        atan_table[10] <= 'b00000000000010100010111110011000;
        atan_table[11] <= 'b00000000000001010001011111001100;
        atan_table[12] <= 'b00000000000000101000101111100110;
        atan_table[13] <= 'b00000000000000010100010111110011;
        atan_table[14] <= 'b00000000000000001010001011111001;
        atan_table[15] <= 'b00000000000000000101000101111100;
        atan_table[16] <= 'b00000000000000000010100010111110;
        atan_table[17] <= 'b00000000000000000001010001011111;
        atan_table[18] <= 'b00000000000000000000101000101111;
        atan_table[19] <= 'b00000000000000000000010100010111;
        atan_table[20] <= 'b00000000000000000000001010001011;
        atan_table[21] <= 'b00000000000000000000000101000101;
        atan_table[22] <= 'b00000000000000000000000010100010;
        atan_table[23] <= 'b00000000000000000000000001010001;
        atan_table[24] <= 'b00000000000000000000000000101000;
        atan_table[25] <= 'b00000000000000000000000000010100;
        atan_table[26] <= 'b00000000000000000000000000001010;
        atan_table[27] <= 'b00000000000000000000000000000101;
        atan_table[28] <= 'b00000000000000000000000000000010;
        atan_table[29] <= 'b00000000000000000000000000000001;
        atan_table[30] <= 'b00000000000000000000000000000000;
        base <= interval_in;
        angle_base <= base_in;
        angle <= 0;
        iter <= count;
        readFlag <= 1;
        //flag <= 0;
        //count1 <= 0;
        //count2 <= 0;
        //count3 <= 0;
    endmethod
    method Action next_iter() if(count1 == samp);
        proceed <= 1;
    endmethod
    method Action readbram(Bit#(16) address) if(count1== samp);
        cosine.portA.request.put(makeRequest(False,address,?));
        sine.portA.request.put(makeRequest(False,address,?));
    endmethod
    method ActionValue#(Int#(30)) bramcosine;
        let cosine_val <- cosine.portA.response.get;
        return cosine_val;
    endmethod
    method ActionValue#(Int#(30)) bramsine;
        let sine_val <- sine.portA.response.get;
        return sine_val;
    endmethod
endmodule : mkcordicip

endpackage : cordicip