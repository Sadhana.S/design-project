/*
Module : Cordic implementation for frequency shift
Description : 
*/
package cordicpipe_acq;

import FIFO::*;
import SpecialFIFOs ::*;
import Vector ::*;
import Real ::*;
import FixedPoint::*;
import BRAM::*;
import BRAMCore::*;
import GetPut::*;
import ClientServer::*;

typedef 16367 Samples;
typedef 16 Iterations;
typedef 17 Iterations1;//Iterations + 1
typedef 4096 Scale;//Required scalling factor*16

interface Ifc_cordicpipe_acq;
    method Action initialise(Int#(33) base_in, Int#(33) interval_in);//Initialisation of cordic table
    method Action reinitialise;
    method Action bram_read(Bit#(11) address_in);
    method ActionValue#(Int#(16)) bram_i;
    method ActionValue#(Int#(16)) bram_q;
endinterface : Ifc_cordicpipe_acq

function BRAMRequest#(Bit#(11), Int#(16)) makeRequest(Bool write, Bit#(11) addr, Int#(16) data);
    return BRAMRequest{
    write: write,
    responseOnWrite:False,
    address: addr,
    datain: data
    };
 endfunction

module mkcordicpipe_acq(Ifc_cordicpipe_acq);

    Integer samp1 = valueOf(Samples);
    Bit#(16) samp = fromInteger(samp1);
    Integer iterations = valueOf(Iterations);
    Bit#(16) cordic_iter = fromInteger(iterations);
    Integer scale = valueOf(Scale);
    Int#(15) scale1 = fromInteger(scale);
    FixedPoint#(1,20) mult_base = 0.0625;

    Vector#(31,Reg#(Int#(32))) atan_table <- replicateM(mkReg(0));
    Vector#(Iterations1,FIFO#(Tuple3 #(Int#(16),Int#(16),Int#(32)))) pipe <- replicateM(mkPipelineFIFO);
    FIFO#(Tuple3 #(Int#(4), Int#(4), Int#(33))) f1 <- mkPipelineFIFO;  // alter i and q values
    FIFO#(Tuple3 #(Int#(16), Int#(16), Int#(33))) f2 <- mkPipelineFIFO;  // converge angle within -pi/2 to pi/2;
    
    BRAM_Configure cfg1 = defaultValue;
    BRAM1Port#(Bit#(11),Int#(16)) inphase <- mkBRAM1Server(cfg1);
    BRAM1Port#(Bit#(11),Int#(16)) quadphase <- mkBRAM1Server(cfg1);
    Reg#(Bit#(11)) count1 <- mkReg(0);

    BRAM_PORT#(Bit#(36),Int#(4)) i_val <- mkBRAMCore1Load(samp1,False,"/home/sadhana/Desktop/NavIC_correlator/src/I_val",False);
    BRAM_PORT#(Bit#(36),Int#(4)) q_val <- mkBRAMCore1Load(samp1,False,"/home/sadhana/Desktop/NavIC_correlator/src/q_value",False);  
    Reg#(Bit#(36)) count2 <- mkReg(0);
 
    Reg#(FixedPoint#(1,8)) mult2 <- mkReg(0.60725320630464);
    Reg#(Int#(33)) angle <- mkReg(0);
    Reg#(Int#(33)) angle_base <- mkReg(0);
    Reg#(Int#(33)) interval <- mkReg(0);
    Reg#(FixedPoint#(1,20)) mult_hold <- mkReg(0);

    Reg#(Int#(16)) accumulate_I <- mkReg(0);
    Reg#(Int#(16)) accumulate_Q <- mkReg(0);

    Reg#(Bit#(2)) flag <- mkReg(0);
    Reg#(Bit#(1)) readFlag <- mkReg(0);
    Reg#(Bit#(1)) rg_proceed <- mkReg(0);

    rule r0_get_inputs1(readFlag==1);//Address to i_val and q_val BRAMs and angle accumulation
        if(count2 < zeroExtend(samp))
        begin
            i_val.put(False,count2,?);
            q_val.put(False,count2,?);
            Int#(34) temp = zeroExtend(angle) + zeroExtend(angle_base);
            if(temp >= 4294967295)
                angle <= truncate(temp - 4294967296);
            else
                angle <= truncate(temp);
        end
        if(count2 == 0)
            flag <= 1;
        count2 <= count2 + 1;
    endrule
    rule r0_get_inputs2(flag == 1);//Values from i_val and q_val BRAMs
        if((count2 == zeroExtend(samp)+1)||(count2 == zeroExtend(samp) + 2))
            f1.enq(tuple3(0,0,0));
        else if(count2 == zeroExtend(samp)+3)
            flag <= 2;
        else
        begin
            Int#(4) i_in = i_val.read;
            Int#(4) q_in = q_val.read;
            Int#(33) angle_in = angle;
            f1.enq(tuple3(i_in,q_in,angle_in));
        end
    endrule
    rule r1_add_guards;//Input_scalling
        match {.i1, .q1, .a1} = f1.first; f1.deq;
        FixedPoint#(4,1) i2 = fromInt(i1);
        FixedPoint#(4,1) q2 = fromInt(q1);
        FixedPoint#(5,9) i3 = fxptMult(i2,mult2);
        FixedPoint#(5,9) q3 = fxptMult(q2,mult2);
        FixedPoint#(15,1) mult1 = fromInt(scale1);
        Int#(16) i = truncate(fxptGetInt(fxptMult(i3,mult1)));
        Int#(16) q = truncate(fxptGetInt(fxptMult(q3,mult1)));
        f2.enq(tuple3(i,q,a1));
    endrule
    rule r2_converge_angle;//Converging all the angles within first quadrant for cordic operations
        match {.i, .q, .a2} = f2.first; f2.deq;
        Int#(16) i4 = 0;
        Int#(16) q4 = 0;
        Int#(32) a3 = 0;
        if((a2 > 1073741824)&&(a2 <= 2147483648)) //second quadrant
        begin
            i4 = -q;
            q4 = i;
            a3 = truncate(a2 + 3221225472);
        end
        else if((a2 > 2147483648)&&(a2 <= 3221225472)) //third quadrant
                    begin
                        i4 = -i;
                        q4 = -q;
                        a3 = truncate(a2 + 2147483648);
                    end
                    else if((a2 > 3221225472)&&(a2<=4294967295)) //fourth quadrant
                        begin
                            i4 = q;
                            q4 = -i;
                            a3 = truncate(a2 + 1073741824);
                        end
                        else
                        begin //first quadrant
                            i4 = i;
                            q4 = q;
                            a3 = truncate(a2);
                        end
        pipe[0].enq(tuple3 (i4,q4,a3));
    endrule
    for(Bit#(16) j=0; j<cordic_iter; j=j+1)//Cordic iterations
    begin
        rule r3_processing;
            match {.x,.y,.z} = pipe[j].first; pipe[j].deq;
            let a2=x >> j;
            let a3=y >> j;
            pipe[j+1].enq(tuple3 (((z>0)?x-a3:x+a3),((z>0)?y+a2:y-a2),((z>0)?z-atan_table[j]:z+atan_table[j])));
        endrule
    end
    rule r4_accumulate_and_push_output(count1<=2046);//Accumulation of cordic outputs - two values for each PRN resulting in 2046 values
        match{.i1,.q1,.angle1} = pipe[cordic_iter].first;pipe[cordic_iter].deq;
        Int#(12) i_out = truncate(i1>>4);
        Int#(12) q_out = truncate(q1>>4);
        if(count1 == 0)
        begin
            accumulate_I <= signExtend(i_out);
            accumulate_Q <= signExtend(q_out);
            count1 <= count1 + 1;
        end
        else
        begin
        FixedPoint#(1,20) temp1 = mult_hold + mult_base;
        //fxptWrite(10,temp1);
        //$display("\n");
        UInt#(20) temp2 = fxptGetFrac(temp1);
        mult_hold <= temp1;
        UInt#(20) temp3 = 524288 + fxptGetFrac(mult_base);
        if((((temp2 >= 524288)&&(temp2 < temp3))||(temp2<fxptGetFrac(mult_base))))
        begin
            inphase.portA.request.put(makeRequest(True,count1-1,accumulate_I));
            quadphase.portA.request.put(makeRequest(True,count1-1,accumulate_Q));
            accumulate_I <= signExtend(i_out);
            accumulate_Q <= signExtend(q_out);
            //$display($time,"count1 = %d : accumulate_I = %d and accumulate_Q = %d \n",count1,accumulate_I,accumulate_Q);
            count1 <= count1 + 1;
        end
        else
        begin
            accumulate_I <= accumulate_I + signExtend(i_out);
            accumulate_Q <= accumulate_Q + signExtend(q_out);
        end
        end
    endrule

    method Action initialise(Int#(33) base_in, Int#(33) interval_in);
//Feedback angles for cordic
        atan_table[00] <= 'b00100000000000000000000000000000; // 45.000 degrees -> atan(2^0)
        atan_table[01] <= 'b00010010111001000000010100011101; // 26.565 degrees -> atan(2^-1)
        atan_table[02] <= 'b00001001111110110011100001011011; // 14.036 degrees -> atan(2^-2)
        atan_table[03] <= 'b00000101000100010001000111010100; // atan(2^-3)
        atan_table[04] <= 'b00000010100010110000110101000011;
        atan_table[05] <= 'b00000001010001011101011111100001;
        atan_table[06] <= 'b00000000101000101111011000011110;
        atan_table[07] <= 'b00000000010100010111110001010101;
        atan_table[08] <= 'b00000000001010001011111001010011;
        atan_table[09] <= 'b00000000000101000101111100101110;
        atan_table[10] <= 'b00000000000010100010111110011000;
        atan_table[11] <= 'b00000000000001010001011111001100;
        atan_table[12] <= 'b00000000000000101000101111100110;
        atan_table[13] <= 'b00000000000000010100010111110011;
        atan_table[14] <= 'b00000000000000001010001011111001;
        atan_table[15] <= 'b00000000000000000101000101111100;
        atan_table[16] <= 'b00000000000000000010100010111110;
        atan_table[17] <= 'b00000000000000000001010001011111;
        atan_table[18] <= 'b00000000000000000000101000101111;
        atan_table[19] <= 'b00000000000000000000010100010111;
        atan_table[20] <= 'b00000000000000000000001010001011;
        atan_table[21] <= 'b00000000000000000000000101000101;
        atan_table[22] <= 'b00000000000000000000000010100010;
        atan_table[23] <= 'b00000000000000000000000001010001;
        atan_table[24] <= 'b00000000000000000000000000101000;
        atan_table[25] <= 'b00000000000000000000000000010100;
        atan_table[26] <= 'b00000000000000000000000000001010;
        atan_table[27] <= 'b00000000000000000000000000000101;
        atan_table[28] <= 'b00000000000000000000000000000010;
        atan_table[29] <= 'b00000000000000000000000000000001;
        atan_table[30] <= 'b00000000000000000000000000000000;
        angle_base <= base_in;
        interval <= interval_in;
        accumulate_I <= 0;
        accumulate_Q <= 0;
        mult_hold <= 0;
        count1 <= 0;
        count2 <= 0;
        readFlag <= 1;
        flag <= 0;
        angle <= 0;
        $display($time, " CORDIC started \n");
    endmethod
    method Action reinitialise if(count1 == 2047);
        accumulate_I <= 0;
        accumulate_Q <= 0;
        mult_hold <= 0;
        count1 <= 0;
        count2 <= 0;
        readFlag <= 1;
        flag <= 0;
        angle <= 0;
        angle_base <= angle_base + interval;
    endmethod
    method Action bram_read(Bit#(11) address_in) if(count1 == 2047);
        inphase.portA.request.put(makeRequest(False,address_in,?));
        quadphase.portA.request.put(makeRequest(False,address_in,?));
    endmethod
    method ActionValue#(Int#(16)) bram_i if(count1 == 2047);
        let cosine_val <- inphase.portA.response.get;
        return cosine_val;
    endmethod
    method ActionValue#(Int#(16)) bram_q if(count1 == 2047);
        let sine_val <- quadphase.portA.response.get;
        return sine_val;        
    endmethod
endmodule : mkcordicpipe_acq

endpackage : cordicpipe_acq