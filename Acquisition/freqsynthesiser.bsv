//This package gives the base angle and the interval angle which has to be added every time in the angle accumulator
//Input : Scalled Sampling frequency (2^32/sampling frequency), Doppler frequency shift interval, base frequency(the base doppler frequency such as Intermediate Frequency(IF)-10 KHz or -10KHz for acquisition) and the phase to be added if any
//Output : Base angle in scalled version of Base frequency and the interval angle in scalled version of Doppler frequency interval

//1. The scaling factor is left shifted by 31 to make it an Integer (from Fixed Point) so as to avoid fixed point computations thereafter.
//2. We do not actually compute (2*pi*fd/fs). Angle lookup happens in degrees, and not radians. Therefore, we need to compute (2*pi*fd/fs)*(180/pi)= (2*fd*180/fs). Also,
//   since there is a scaling factor, where 360 degrees corresponds to 2^32 => 180 degrees corresponds to 2^31, we need to compute (2*fd*180/fs)*(2^31)/180 = (2*fd/fs)*(2^31)= (fd/fs)<<32
//3. TODO instead of computing fd/fs, store 1/fs which you can use for multiplication with fd later. This would save cycles, and also when sampling freq. is a parameter it would save a divider
//4. TODO change read_flag<=0 update to the previous rule. By doing this, we can take in new inputs one cycle earlier.

package freqsynthesiser;

	import Real::*;
	import FixedPoint::*;
	import Vector::*;
	import BRAMCore::*;
	
	interface Ifc_freqsynthesiser;
	
		//interval_in: Doppler freq interval. For acquisition, interval is 500Hz for now.
		//samp_freq_in(fs): 12MHz(value is 12 x 10^6). Usually >10MHz. Fixed for a design. May be can be changed to a parameter rather than an input.
		//base_freq_in: Starting freq. For acquisition, it is -10kHz. 
		//phase_in: Phase to be added. In acquisition, it is 0. In tracking, phase is provided by the PLL.
		method Action get_inputs(Int#(16) interval_in, FixedPoint#(12,20) scalled_samp_freq_in, FixedPoint#(24,8) base_freq_in, Int#(33) phase_in);
		method Int#(33) results_base();			//Scaled version of base freq. Scaled by ((2*pi/fs)<<31)
		method Int#(33) results_interval();		//Scaled version of the interval_in.
		
	endinterface : Ifc_freqsynthesiser
	
	module mkfreqsynthesiser(Ifc_freqsynthesiser);
	
		Reg#(Bit#(1)) readFlag <- mkReg(0);
		Reg#(Bit#(3)) flag <- mkReg(0);
	
		Reg#(Int#(16)) interval <- mkReg(0);
		Reg#(FixedPoint#(24,8)) base_freq <- mkReg(0);
		Reg#(FixedPoint#(12,20)) scalled_samp_freq <- mkReg(0);
		Reg#(Int#(33)) phase <- mkReg(0);//Scalled version in degrees
		Reg#(Int#(33)) interval_angle <- mkReg(0);
		Reg#(Int#(33)) base_angle <- mkReg(0);

		rule r1_freq_calc(flag == 1);//computation of base_freq*scalled sampling frequency and interval_freq*scalled sampling frequency
			FixedPoint#(24,1) temp0 = fromInt(interval);
			Int#(36) temp1 = fxptGetInt(fxptMult(scalled_samp_freq,base_freq));
			Int#(36) temp2 = fxptGetInt(fxptMult(scalled_samp_freq,temp0));
			Int#(33) temp3 = truncate(temp1)+phase;
			//This is done to make the angle a positive value between 0 to 2pi. Note that it is not same as taking 2's complement.
			//For example, -pi/2 should become 3pi/2 and not +pi/2.
			//To make the angle a postive value between 0 to 2pi, we add (2^32)-1 i.e. 2pi radians to it.
			//TODO instead add (2^32) which implies that it is enough if you send in the lower 32 bits with the 33rd bit set as 0.
			if(temp3 < 0)
				temp3 = temp3 + 4294967295;
			$display($time,"Base angle : %d \n",temp3);
			$display($time,"Interval angle : %d\n",temp2);
			interval_angle <= truncate(temp2);
			base_angle <= temp3;
			flag <= 0;
		endrule
		
		method Action get_inputs(Int#(16) interval_in, FixedPoint#(12,20) scalled_samp_freq_in, FixedPoint#(24,8) base_freq_in, Int#(33) phase_in) ;
			interval <= interval_in;
			scalled_samp_freq <= scalled_samp_freq_in;
			base_freq <= base_freq_in;
			$display($time,"Base frequency : %d",fxptGetInt(base_freq_in));
			phase <= phase_in;
			flag <= 1;
		endmethod
		method Int#(33) results_base if(flag == 0);
			return base_angle;
		endmethod
		method Int#(33) results_interval if(flag == 0);
			return interval_angle;
		endmethod
	endmodule : mkfreqsynthesiser
endpackage