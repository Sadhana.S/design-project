/*
Module : Magnitude computation using CORDIC
Description : 
Input : Results from correlation of In_phase(I) and Quadrature Phase(Q)
Output : sqrt(I^2 + Q^2)*1.64676
    The constant 1.64676 is introduced by the cordic iterations. The constant can be ignored as the output is obtained by the ratio(first_max/second_max) and gets cancelled
*/
package magnitudepipe;

import Vector::*;
import FIFO::*;
import SpecialFIFOs::*;

interface Ifc_magnitudepipe;
    method Action get_inputs(Int#(32) i_in, Int#(32) q_in);
    method ActionValue#(Int#(32)) result_magnitude();
endinterface : Ifc_magnitudepipe

module mkmagnitudepipe(Ifc_magnitudepipe);

    Vector#(17,FIFO#(Tuple2 #(Int#(35),Int#(35)))) processing_pipe <- replicateM(mkPipelineFIFO);

    for(Bit#(5) j = 0; j < 16; j = j+1)
    begin
        rule r1_cordic_processing;
            match{.magnitude,.y} = processing_pipe[j].first; processing_pipe[j].deq;
            //$display($time," %d Intermediate output : %d and %d \n",j,magnitude,y);
            let a = magnitude >> j;
            let b = y >> j;
            processing_pipe[j+1].enq(tuple2((y<0)?magnitude-b:magnitude+b,(y<0)?y+a:y-a));
        endrule
    end

    method Action get_inputs(Int#(32) i_in, Int#(32) q_in);
        processing_pipe[0].enq(tuple2((i_in<0)?signExtend(-i_in):signExtend(i_in),(i_in<0)?signExtend(-q_in):signExtend(q_in)));
    endmethod
    method ActionValue#(Int#(32)) result_magnitude();
        match{.mag_out,.y_out} = processing_pipe[16].first; processing_pipe[16].deq;
        Int#(32) magnitude = truncate(mag_out);
        return magnitude;
    endmethod
endmodule : mkmagnitudepipe
endpackage : magnitudepipe